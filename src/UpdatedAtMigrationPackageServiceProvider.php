<?php

namespace cityfibre\updatedAtMigrationPackage;

use cityfibre\updatedAtMigrationPackage\Console\CreateUpdatedAtMigration;
use Illuminate\Support\ServiceProvider;

class UpdatedAtMigrationPackageServiceProvider extends ServiceProvider
{

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands(
                [
                    CreateUpdatedAtMigration::class,
                ]
            );
        }
    }

    public function register()
    {
    }
}
