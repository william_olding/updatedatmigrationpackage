<?php

namespace cityfibre\updatedAtMigrationPackage\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;


class CreateUpdatedAtMigration extends Command
{
    protected $signature = 'CreateUpdatedAtMigration';

    protected $description = 'creates a migration for all the tables in this application to set updatedAt to Default to now';

    private $stubPath = '/stubs/MigrationTemplate.stub';

    private $excludeTables = [
        'migrations',
        'failed_jobs'
    ];

    public function handle()
    {
        $files = new Filesystem();
        $tables = DB::select('SHOW TABLES');
        $string = 'Tables_in_' . env('DB_DATABASE');
        foreach ($tables as $table) {
            $table->name = $table->$string;
            if (!in_array($table->name, $this->excludeTables)) {
                $Migration = str_replace('Dummy', $table->name, $files->get($this->getStub()));
                file_put_contents(base_path('database/migrations/'.date_format(Carbon::now(),"Y_m_d_His") .'_alter_timestamp_'. $table->name . '.php'), $Migration);
            }
        }
    }

    protected function getStub()
    {
        return dirname(__FILE__, 2) . $this->stubPath;
    }
}
